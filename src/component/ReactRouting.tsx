import { BrowserRouter, Route, Routes } from 'react-router-dom';
import App from '../page/App';
import { Create } from '../page/create/Create';
import License from '../page/license/License';
// import { Job } from '../page/job/Job';

const ReactRouting = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={`/`} element={<App />}></Route>
        <Route path={`/Create`} element={<Create />}></Route>
        <Route path={`/Licence`} element={<License />}></Route>
        {/* <Route path={`/Job`} element={<Job />}></Route> */}
      </Routes>
    </BrowserRouter>
  );
};

export default ReactRouting;
