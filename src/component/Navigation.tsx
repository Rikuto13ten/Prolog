import { HomeIcon, LogoutIcon, PlusIcon } from '@heroicons/react/solid';
import React from 'react';
import { Link } from 'react-router-dom';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import { Create } from '../page/create/Create';
import { JobLists } from '../page/home/JobLists';
import { auth } from './firebase';

export const Navigation = () => {
  return (
    <>
      <Tabs className={'mb-4 flex flex-col'}>
        <TabList
          className={
            'mb-7 justify-center gap-5 text-sm font-bold sm:flex max-sm:hidden'
          }
        >
          <Tab
            className={
              'cursor-pointer rounded-t-md p-1 transition hover:text-blue-500'
            }
            selectedClassName={'border-b-2 border-blue-500 text-blue-500'}
          >
            目標一覧
          </Tab>
          <Tab
            className={
              'cursor-pointer rounded-t-md p-1 transition hover:text-blue-500'
            }
            selectedClassName={'border-b-2 border-blue-500 text-blue-500'}
          >
            目標作成
          </Tab>
        </TabList>

        <div className="">
          <TabPanel>
            <JobLists></JobLists>
          </TabPanel>
          <TabPanel>
            <Create></Create>
          </TabPanel>
        </div>

        <div className="text-blue-grey-400 font-progress-number mb-8 h-fit w-full text-center tracking-wider">
          <Link to={`/Licence`} className="">
            license
          </Link>
          <p>@ Rikuto Noda. All rights Reserved. </p>
        </div>

        {/* タブ */}
        <TabList
          className={
            'fixed bottom-0 left-0 order-2 mx-auto flex  w-full justify-center gap-4 border border-sky-400/10 bg-white px-4 py-2 text-xs font-bold shadow-nav shadow-blue-400/10 sm:hidden'
          }
        >
          <Tab
            className={
              'flex cursor-pointer flex-col items-center gap-1 p-2 text-gray-300'
            }
            selectedClassName={'text-blue-400'}
          >
            <HomeIcon className="h-5 w-5" />
          </Tab>
          <Tab
            className={
              'flex cursor-pointer flex-col items-center gap-1 p-2 text-gray-300'
            }
            selectedClassName={'text-blue-400'}
          >
            <PlusIcon className="h-5 w-5"></PlusIcon>
          </Tab>
          <button
            onClick={() => auth.signOut()}
            className="flex aspect-square cursor-pointer flex-col items-center gap-1 rounded-full p-2 font-bold text-gray-300 "
          >
            <LogoutIcon className="h-5 w-5"></LogoutIcon>
          </button>
        </TabList>
      </Tabs>
    </>
  );
};
