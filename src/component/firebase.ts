import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { getFirestore } from 'firebase/firestore';

const firebaseApp = {
  apiKey: 'AIzaSyA-S8Pf4dOHJLr_DwujoYHikNvujb1Fosg',
  authDomain: 'prolog-9ea36.firebaseapp.com',
  projectId: 'prolog-9ea36',
  storageBucket: 'prolog-9ea36.appspot.com',
  messagingSenderId: '483610740673',
  appId: '1:483610740673:web:5ced2e02a1c5df9fdbd140',
  measurementId: 'G-S2DY1SBXG1',
};

firebase.initializeApp(firebaseApp);

const app = initializeApp(firebaseApp);
const db = getFirestore(app);

const auth: any = firebase.auth();
const myId = getAuth().currentUser?.uid;

export { db, auth, myId, app };
