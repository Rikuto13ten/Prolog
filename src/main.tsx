import React from 'react';
import ReactDOM from 'react-dom/client';
import ReactRouting from './component/ReactRouting';
import './index.css';
import './index.css';
import App from './page/App';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ReactRouting />
  </React.StrictMode>
);
