import { type } from 'os';
import React, { useEffect } from 'react';

type props = {
  title: string;
  unit: string;
};

export const TitleValidation = (title: string) => {
  const validation = () => {
    return title ? 'opacity-0' : 'opacity-100';
  };
  return (
    <p className={validation + 'text-red-700'}>目標が入力されていません</p>
  );
};
