import { type } from 'os';
import React, { useState } from 'react';

type props = {
  setTitle: React.Dispatch<React.SetStateAction<string | undefined>>;
};

export const CreateTitle = (props: props) => {
  const { setTitle } = props;
  return (
    <>
      <h2 className="mb-3 text-xl">目標を決めよう</h2>
      <p className="text-sm">例 : 参考書を読む、走る </p>
      <input
        type="text"
        name="title"
        onChange={(e) => setTitle(e.target.value)}
        className="my-1 mb-6 rounded-md border border-blue-100 bg-white p-2"
      />
    </>
  );
};
