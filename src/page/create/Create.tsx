import { getAuth } from 'firebase/auth';
import firebase from 'firebase/compat';
import { addDoc, collection } from 'firebase/firestore';
import React, { useEffect, useState } from 'react';
import { db } from '../../component/firebase';
import { CreateTitle } from './parts/CreateTitle';
import { TitleValidation } from './parts/Validation';

export const Create: React.FC = () => {
  const [title, setTitle] = useState<string>();
  const [unit, setUnit] = useState<string>();
  const [num, setNum] = useState<number>();

  const handleOnSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (title && unit && num) {
      const colRef = collection(db, 'user');
      const data = {
        title: title,
        unit: unit,
        num: num,
        uid: getAuth().currentUser?.uid,
        timeStamp: firebase.firestore.FieldValue.serverTimestamp(),
        progress: 0, // 達成度
      };
      await addDoc(colRef, data);
      console.log('成功');
      setTitle('');
      setUnit('');
      setNum(0);
      alert('登録しました');
    } else {
      alert('入力されていません');
    }
  };

  return (
    <div className="mt-6 flex h-full justify-center">
      <form onSubmit={handleOnSubmit} className="mb-4 flex flex-col ">
        <CreateTitle setTitle={setTitle} />

        <h2 className="mb-3 text-xl">分割数と単位を決めよう</h2>
        <div className="flex w-full flex-col">
          <p className="text-sm leading-7">
            <span className="rounded-md bg-blue-100 p-1 text-blue-600">
              分割数
            </span>
            <br />
            目標までの道のり
          </p>
          <input
            type="number"
            name="num"
            value={num}
            onChange={(e) => setNum(e.target.valueAsNumber)}
            className={`mb-4 rounded-md border border-blue-100 bg-white p-2`}
            placeholder="分割数"
          />

          <p className="text-sm leading-7">
            <span className="rounded-md bg-blue-100 p-1 text-blue-600">
              単位
            </span>
            <br />
            目標までの道のりを分割したときの単位となるもの
          </p>
          <input
            type="text"
            name="unit"
            value={unit}
            onChange={(e) => setUnit(e.target.value)}
            className="mb-7 rounded-md border border-blue-100 bg-white p-2"
            placeholder="単位"
          />
        </div>
        <button className="w-auto rounded-md bg-blue-600 p-2 text-white">
          送信
        </button>
      </form>
    </div>
  );
};
