import {
  getAuth,
  GoogleAuthProvider,
  inMemoryPersistence,
  setPersistence,
  signInWithPopup,
  signInWithRedirect,
} from 'firebase/auth';
import task from '../images/task.svg';
import signIn from '../images/google.png';

const SignIn = () => {
  const signInWithGoole = () => {
    setPersistence(getAuth(), inMemoryPersistence).then(() => {
      const provider = new GoogleAuthProvider();
      return signInWithPopup(getAuth(), provider);
    });
  };
  return (
    <div className="m-0 box-border flex min-h-screen flex-col items-center justify-center gap-20 lg:flex-row">
      <img src={task} className=" mt-20 w-[80%] max-w-sm" alt="productImage" />
      <div className="flex flex-col items-center">
        <h1 className=" mb-4 text-center text-3xl font-extrabold tracking-wide">
          Prolog
        </h1>
        <div className="mb-8 h-1 w-16 rounded-full bg-blue-300"></div>
        <p className="mb-8 max-w-[15rem] text-sm font-bold leading-6 text-gray-700/50">
          Prologは進捗管理アプリです。自分の目標やタスクの進捗度合いを可視化します。
        </p>
        <button onClick={signInWithGoole} className="w-4/5">
          <img
            src={signIn}
            alt="Sign in with Google"
            className="w-full max-w-sm"
          />
        </button>
      </div>
    </div>
  );
};

export default SignIn;
