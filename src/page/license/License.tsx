import React from 'react';
import { IconContext } from 'react-icons';
import { Link } from 'react-router-dom';
import unicornWow from '../../images/unicorn_wow.png';
import { BsArrowLeft } from 'react-icons/bs';

const License = () => {
  return (
    <div>
      <IconContext.Provider value={{ size: '24px' }}>
        <Link
          to={`/`}
          className="inline-block p-2 hover:bg-blue-200/30 duration-300 rounded-full"
        >
          <BsArrowLeft></BsArrowLeft>
        </Link>
      </IconContext.Provider>
      <h3 className="mt-6">ライセンス表記</h3>
      <div className="mt-6 font-progress-number">
        <li className="list-none">
          <a
            href="https://www.flaticon.com/free-icons/mad"
            title="mad icons"
            className="tracking-wide flex gap-4 items-center p-3 text-blue-600 shadow-xl rounded-lg bg-white shadow-blue-100"
            target={'_blank'}
            rel={'noopener noreferrer'}
          >
            <img src={unicornWow} alt="" className="w-7" />
            Mad icons created by Freepik - Flaticon
          </a>
        </li>
      </div>
    </div>
  );
};

export default License;
