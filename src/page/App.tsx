import React from 'react';
import '../index.css';
import { auth } from '../component/firebase';
import { useAuthState } from 'react-firebase-hooks/auth';
import SignIn from './SignIn';
import { Home } from './home/Home';

const App = () => {
  const [user] = useAuthState(auth);
  return <div className="App">{user ? <Home /> : <SignIn />}</div>;
};

export default App;
