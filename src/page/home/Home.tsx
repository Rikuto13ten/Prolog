import { Navigation } from '../../component/Navigation';

export const Home = () => {
  return (
    <div className="flex flex-col">
      <h1 className="mb-4 mt-5 text-xl font-bold">Prolog</h1>
      <Navigation />
    </div>
  );
};
