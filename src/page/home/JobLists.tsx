import { Slider } from '@mui/material';
import { getAuth } from 'firebase/auth';
import {
  collection,
  deleteDoc,
  doc,
  DocumentData,
  onSnapshot,
  orderBy,
  Query,
  query,
  updateDoc,
  where,
} from 'firebase/firestore';
import { useEffect, useState } from 'react';
import { db } from '../../component/firebase';
import { EditModal } from './parts/EditModal';
import { NoJobList } from './parts/NoJobList';
import { SortSelect } from './parts/SortSelect';

type user = {
  id: string; // documentId
  uid: string; // userId
  title: string; // title
  unit: string; // 単位
  num: number; // 分割数
  progress: number; // 進捗状況
  timeStamp: EpochTimeStamp;
};

export const JobLists = () => {
  const [myJob, setMyJob] = useState<[]>([]);
  const [progress, setProgress] = useState<number | number[]>(0);
  const [documentId, setDocumentId] = useState<string>();
  // クエリ
  const q: Query<DocumentData> = query(
    collection(db, 'user'),
    where('uid', '==', getAuth().currentUser?.uid)
  );

  useEffect(() => {
    (async () => {
      // クエリ

      // クエリ実行
      onSnapshot(q, (snapShot: DocumentData) => {
        setMyJob(
          snapShot.docs.map((doc: DocumentData) => ({
            ...doc.data(),
            id: doc.id,
          }))
        );
      });
    })();
  }, []);

  const delMyjob = async (id: string) => {
    await deleteDoc(doc(db, 'user', id));
    console.log(myJob);
  };

  const updateProgress = (id: string) => {
    const selectDoc = doc(db, 'user', id);
    updateDoc(selectDoc, {
      progress: progress,
    });
  };

  const sliderOnChange = (e: any, value: number | number[]) => {
    setProgress(value);
  };

  const noJobList = () => {
    if (myJob.length == 0) {
      return <NoJobList></NoJobList>;
    }
  };

  return (
    <div className="p-0">
      <h2 className="text-xl">目標一覧</h2>

      {noJobList()}
      <ul className="todo-list-grid py-4">
        {myJob.map((user: user) => (
          <li
            key={user.uid}
            className="shadow-xl flex list-none flex-col rounded-md bg-white px-4 py-2 shadow-blue-200/30"
          >
            <div className="flex items-center justify-between">
              <h2>{user.title}</h2>
              <div className="font-progress-number text-xl font-bold tracking-widest">
                {user.progress > user.num
                  ? (user.progress = user.num)
                  : user.progress}
                /{user.num}
                <span className="ml-1 text-sm">{user.unit}</span>
              </div>
            </div>
            <div className="flex items-baseline justify-end">
              <h3 className="mr-2 text-xs text-gray-500">選択中の進捗度</h3>
              <p className="font-progress-number w-10 text-right font-bold tracking-widest text-gray-500">
                {user.progress}
              </p>
            </div>

            <Slider
              defaultValue={user.progress}
              min={0}
              max={user.num}
              valueLabelDisplay="auto"
              onChange={(e, value) => sliderOnChange(e, value)}
              sx={{
                height: 40,
                padding: '8px, 0',
                '& .MuiSlider-root': {},
                '& .MuiSlider-rail': {
                  color: 'white',
                  boxShadow:
                    'inset 10px 10px 26px #c2c2c2,inset -10px -10px 26px #ffffff;',
                },
                '& .MuiSlider-track': {
                  color: 'transparent',
                  background:
                    'linear-gradient(90deg, rgba(251, 213, 251, 1), rgba(149, 233, 243, 1))',
                  // WebkitBackgroundClip: 'text',
                },
                '& .MuiSlider-thumb': {
                  height: '70%',
                  width: 'auto',
                  aspectRatio: '1/1',
                  color: 'white',
                  opacity: '0',
                },
              }}
            />
            <div className="mb-2 flex justify-between">
              <button
                className="rounded-md bg-blue-500 p-4 py-2 text-xs tracking-wide text-white"
                onClick={() => updateProgress(user.id)}
              >
                進捗度を確定
              </button>
              <EditModal user={user.id} title={user.title} num={user.num} />
            </div>
            <button
              className="self-end px-4 py-2 text-xs font-bold text-red-600"
              onClick={() => delMyjob(user.id)}
            >
              削除
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};
