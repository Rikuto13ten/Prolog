import React from 'react';
import unicornWow from '../../../images/unicorn_wow.png';

export const NoJobList = () => {
  return (
    <div className="w-fit mt-4 mx-auto flex flex-col items-center justify-center gap-8 text-center">
      <img src={unicornWow} className="w-44 opacity-40" />
      <h3 className="text-blue-grey-300">まだ目標が作られてないよ！</h3>
    </div>
  );
};
