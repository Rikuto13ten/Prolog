import { doc, updateDoc } from 'firebase/firestore';
import { Button, Modal, TextInput } from 'flowbite-react';
import React, { useState } from 'react';
import { db } from '../../../component/firebase';

type user = {
  uid: string;
  title: string;
  num: number;
};

export const EditModal = (props: {
  user: string;
  title: string;
  num: number;
}) => {
  const [status, setStatus] = useState<boolean>(false);
  const [title, setTitle] = useState<string>(props.title);
  const [num, setNum] = useState<number>(props.num);

  const onClick = () => {
    setStatus(true);
  };

  const onClose = () => {
    setStatus(false);
  };

  const onSubmit = (user: string, title: string, num: number) => {
    const selectDoc = doc(db, 'user', user);
    updateDoc(selectDoc, {
      title: title,
      num: num,
    });
    async () => {
      setTitle(props.title);
      setNum(props.num);
    };
    onClose();
  };

  return (
    <div className="">
      <button
        className="border-blue-grey-100 rounded-md border-2 px-4 py-2 text-xs font-bold"
        onClick={onClick}
      >
        編集
      </button>
      <Modal show={status} popup={true} onClose={onClose} size="md">
        <Modal.Header></Modal.Header>
        <Modal.Body>
          <div>
            <h3 className="mb-4 text-xl">編集</h3>
            <label htmlFor="title" className="mb-4 text-lg font-bold">
              目標
            </label>
            <TextInput
              id="title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className="mb-6"
            />
            <label htmlFor="num" className="text-lg font-bold">
              分割数
            </label>
            <TextInput
              id="num"
              type={'number'}
              value={num}
              onChange={(e) => setNum(e.target.valueAsNumber)}
            />
            <button
              onClick={() => onSubmit(props.user, title, num)}
              className="mt-6 w-full rounded-md bg-blue-600 p-3 text-white"
            >
              この内容で決定する
            </button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};
