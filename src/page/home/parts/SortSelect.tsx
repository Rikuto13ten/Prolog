import React, { useState } from 'react';

export const SortSelect = (props: { setSort: (arg0: string) => void }) => {
  const onChangeSelect = (index: number) => {
    switch (index) {
      case 0:
        props.setSort('timeStamp');
        break;

      default:
        props.setSort('timeStamp');
        break;
    }
  };

  return (
    <select onChange={(e) => onChangeSelect(e.target.selectedIndex)}>
      <option value="0">新しいタスク</option>
      <option value="1">古いタスク</option>
    </select>
  );
};
