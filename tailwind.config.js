module.exports = {
  content: [
    './index.html',
    './src/**/*.{js,jsx,ts,tsx}',
    'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {},
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
      'max-sm': { max: '639px' },
    },
    boxShadow: {
      nav: '1px 3px 12px 9px rgba(0, 0, 0, 0.1)',
    },
  },
  plugins: [require('flowbite/plugin')],
};
