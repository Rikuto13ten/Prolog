# Prolog

Prolog は進捗管理を助ける WEB アプリです。

## サイト URL

https://prolog-9ea36.web.app/

## 使用言語、ライブラリ、フレームワーク

JavaScript , TypeScript , React.js , Firebase

## 制作のきっかけ

進捗管理のアプリは数多くあるが、視覚的に自分の進捗度合いが見えるアプリはあまりないと感じた。そこで視覚的に進捗度を見せることでモチベーションの維持、向上を図るために今回制作を始めた。

## 今後の予定

- ソート機能の追加
- バックエンドをFirebaseで実装しているがPHPの学習を進め、PHPで実装していきたい。
- ReactNativeによってネイティブアプリとして落とし込みたい
- TypeScriptに書き換え
